<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::match(['get', 'post'], '/', [\App\Http\Controllers\User\SiteController::class, 'index'])->name('home');
Route::get('/plans', [\App\Http\Controllers\User\SiteController::class, 'plans'])->name('home.plans');


//cabinet start
Route::group(['prefix' => 'cabinet', 'middleware' => 'auth'], function () {
    Route::get('/', [\App\Http\Controllers\User\CabinetController::class, 'index'])->name('cabinet.index');

    Route::group(['prefix' => 'publication'], function () {
        Route::get('/', [\App\Http\Controllers\User\PublicationController::class, 'index'])->name('publication.index');
        Route::get('/create', [\App\Http\Controllers\User\PublicationController::class, 'create'])->name('publication.create');
        Route::post('/store', [\App\Http\Controllers\User\PublicationController::class, 'store'])->name('publication.store');
        Route::get('/{publication}', [\App\Http\Controllers\User\PublicationController::class, 'show'])->name('publication.show');

        Route::post('/{publication}/changeStatus', [\App\Http\Controllers\User\PublicationController::class, 'changeStatus'])->name('publication.changeStatus');
    });
});
//cabinet end


//cart start
Route::group(['prefix' => 'cart'], function () {
    Route::get('/', [\App\Http\Controllers\User\CartController::class, 'index'])->name('cart.index');
    Route::post('/{plan}/add', [\App\Http\Controllers\User\CartController::class, 'add'])->name('cart.add');
    Route::delete('/{plan}', [\App\Http\Controllers\User\CartController::class, 'delete'])->name('cart.delete');
    Route::get('/paymentMethod', [\App\Http\Controllers\User\CartController::class, 'paymentMethod'])->name('cart.paymentMethod');
});
//cart end


//payment start
Route::post('/payment', [\App\Http\Controllers\User\PaymentController::class, 'payment'])->name('make.payment');
Route::get('/success', [\App\Http\Controllers\User\PaymentController::class, 'success'])->name('make.success');

Route::get('/auth/facebook', [\App\Http\Controllers\Auth\FacebookController::class, 'redirectToFacebook'])->name('auth.facebook');
Route::get('/auth/facebook/callback', [\App\Http\Controllers\Auth\FacebookController::class, 'handleFacebookCallback']);

Route::get('/auth/google', [\App\Http\Controllers\Auth\GoogleController::class, 'signInwithGoogle']);
Route::get('/callback/google', [\App\Http\Controllers\Auth\GoogleController::class, 'callbackToGoogle']);

Route::get('/auth/twitter', [\App\Http\Controllers\Auth\TwitterController::class, 'loginwithTwitter']);
Route::get('/auth/callback/twitter', [\App\Http\Controllers\Auth\TwitterController::class, 'cbTwitter']);
//payment end

//auth start
Route::group(['prefix' => 'login'], function () {
    Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('auth.login');
    Route::post('/store', [\App\Http\Controllers\Auth\LoginController::class, 'store'])->name('auth.login.store');
});

Route::group(['prefix' => 'register'], function () {
    Route::get('/', [\App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('auth.register');
    Route::post('/store', [\App\Http\Controllers\Auth\RegisterController::class, 'store'])->name('auth.register.store');
});

Route::get('/logout', [\App\Http\Controllers\Auth\LogoutController::class, 'logout'])->name('auth.logout');

//auth end

//admin start
Route::group(['prefix' => 'admin'], function () {

    Route::get('/login', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'login'])->name('admin.auth.login');
    Route::post('/store', [\App\Http\Controllers\Admin\Auth\LoginController::class, 'store'])->name('admin.auth.login.store');
    Route::get('/logout', [\App\Http\Controllers\Admin\Auth\LogoutController::class, 'logout'])->name('admin.auth.logout');

    Route::group(['prefix' => 'plan', 'middleware' => 'auth_admin'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\Plan\PlanController::class, 'index'])->name('admin.plan.index');
        Route::get('/create', [\App\Http\Controllers\Admin\Plan\PlanController::class, 'create'])->name('admin.plan.create');
        Route::post('/store', [\App\Http\Controllers\Admin\Plan\PlanController::class, 'store'])->name('admin.plan.store');
        Route::post('/{plan}/changeStatus', [\App\Http\Controllers\Admin\Plan\PlanController::class, 'changeStatus'])->name('admin.plan.changeStatus');

    });


});
//admin end
Route::get('/{publication}', [\App\Http\Controllers\User\SiteController::class, 'publicationShow'])->name('home.publication.show');

Route::get('/privacy', function () {
    return view('privacy');
})->name('privacy');

Route::get('/terms', function () {
    return view('terms');
})->name('terms');
