@extends('layouts.main')



@section('content')

    @if(count($allPlans) > 0)
        @foreach($allPlans as $plan)

            <div class="mt-5">
                <div class="card">
                    <h5 class="card-header">{{$plan->id}}</h5>
                    <div class="card-body">
                        <h5 class="card-title">{{$plan->name}}</h5>
                        <p class="card-text">Price: {{$plan->price}} $</p>
                        <p class="card-text">Available Publications: {{$plan->available_publications}}</p>
                        @if(\Illuminate\Support\Facades\Auth::check())
                            @if(!\Illuminate\Support\Facades\Auth::user()->active_plan_id)
                                <form action="{{route('cart.add' , $plan->id)}}" method="post">
                                    @csrf
                                    <button class="btn btn-success">Buy</button>
                                </form>
                            @endif
                        @else
                            <a href="{{route('auth.register')}}" style="margin-right: 20px">Sign up</a>
                            <a href="{{route('auth.login')}}" style="margin-right: 20px">Sign in</a>
                        @endif
                    </div>
                </div>
            </div>

        @endforeach
    @else
        <div class="container" style="text-align: center;margin: 100px auto">
            <h4>List of plans is empty!</h4>
            <a href="{{route('admin.plan.index')}}">Admin Panel</a>
        </div>
    @endif

@endsection
