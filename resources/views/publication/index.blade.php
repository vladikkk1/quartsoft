@extends('layouts.main')


@section('content')

    <div class="container">
        <div style="margin-top: 20px">
            @if(isset($user->active_plan_id))
                <a href="{{route('publication.create')}}">Create publication</a>
            @else
                <a href="{{route('home.plans')}}">Buy Plan</a>
            @endif
        </div>
        <table class="table mt-2">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Content</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($publicationsList as $publication)
                <tr>
                    <th scope="row">{{$publication['id']}}</th>
                    <td>{{$publication['title']}}</td>
                    <td>{{$publication['content']}}</td>
                    <td>{{$publication['status']}}</td>
                    <td><a href="{{route('publication.show' , $publication->id)}}">Show</a></td>
                    <td>
                        <form action="{{route('publication.changeStatus' , $publication['id'])}}" method="post">
                            @csrf
                            <button value="{{$publication['status'] == 'active' ? 'draft' : 'active'}}" name="status"
                                    class="btn btn-primary">Change Status
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            @error('status')
            <p style="color: red;margin-top: 20px">{{$message}}</p>
            @enderror
            </tbody>
        </table>
    </div>
@endsection
