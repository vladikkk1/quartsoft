<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <link rel="stylesheet" type="text/css"
          href="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/css/bootstrap.min.css"></link>
    <link rel="stylesheet" type="text/css"
          href="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/css/prettify.css"></link>
    <link rel="stylesheet" type="text/css"
          href="https://jhollingworth.github.io/bootstrap-wysihtml5//src/bootstrap-wysihtml5.css"></link>
</head>
<body class="wysihtml5-supported">

<div class="container">

    <div class="row" style="margin-top: 50px;">
        <div style="margin-bottom: 20px">
        <a href="{{route('cabinet.index')}}">Back to cabinet</a>
        </div>
        <div class="col-md-12">
            <div id="showimages"></div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="image-upload" method="post" action="{{route('publication.store')}}"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" value="{{old('title')}}" name="title" class="form-control"/>
                            @error('title')
                            <p style="color: red">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea name="content" class="textarea" style="width: 730px; height: 200px">{{old('content')}}</textarea>
                            @error('textarea')
                            <p style="color: red">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="d-flex">
                                <label for="">Status</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" checked name="status" id="inlineRadio1"
                                           value="active">
                                    <label class="form-check-label" for="inlineRadio1">Active</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="inlineRadio2"
                                           value="draft">
                                    <label class="form-check-label" for="inlineRadio2">Inactive</label>
                                </div>
                                @error('status')
                                <p style="color: red">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-sm">Save</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/js/wysihtml5-0.3.0.js"></script>
<script src="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/js/jquery-1.7.2.min.js"></script>
<script src="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/js/prettify.js"></script>
<script src="https://jhollingworth.github.io/bootstrap-wysihtml5//lib/js/bootstrap.min.js"></script>
<script src="https://jhollingworth.github.io/bootstrap-wysihtml5//src/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.textarea').wysihtml5();
    });
</script>
</body>
</html>
