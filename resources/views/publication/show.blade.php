@extends('layouts.main')


@section('content')

    <a href="{{url()->previous()}}">Back</a>

    <div class="container mt-5">
        {{$publication->title}}

        {!! $publication->content !!}
    </div>

@endsection
