@extends('layouts.main')


@section('content')

    @if(count($publications) > 0)
        <form action="{{route('home')}}" method="post">
            @csrf
            <div class="d-flex justify-content-start mt-5">
                <div>
                    <select name="user_id" class="form-control">
                        <option value="">Default</option>
                        @foreach($users as $user)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <button class="btn btn-warning" style="margin-left: 20px">Filter</button>
                </div>
            </div>
        </form>

        <div class="mt-5">
            @foreach($publications as $publication)
                <a href="{{route('home.publication.show' , $publication->id)}}" style="text-decoration: none;color: black">
                    <div class="card mt-5">
                        <div class="card-header">
                            {{$publication->title}}
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                {!! substr($publication->content,0,200) !!}
                                <footer class="blockquote-footer mt-5"><cite
                                        title="Source Title">{{$publication->name}}</cite>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    @else
        <div class="container" style="text-align: center;margin: 100px auto">
            <h4>List of publications is empty!</h4>
            <a href="{{route('admin.plan.index')}}">Admin Panel</a>
        </div>
    @endif
@endsection
