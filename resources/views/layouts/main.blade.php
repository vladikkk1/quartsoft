<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS (jsDelivr CDN) -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Bootstrap Bundle JS (jsDelivr CDN) -->
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>

    <title>Document</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('home')}}">Publications</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('home.plans')}}">Plans</a>
                </li>
            </ul>
        </div>

        @if(\Illuminate\Support\Facades\Auth::check())
            @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
                <a href="{{route('admin.plan.index')}}" style="margin-right: 20px;text-decoration: none">AdminPanel</a>
            @endif
            <a href="{{route('cabinet.index')}}" style="margin-right: 20px;text-decoration: none">Cabinet</a>
            <a href="{{route('auth.logout')}}" style="margin-right: 20px;text-decoration: none">Exit</a>
        @else
            <a href="{{route('auth.register')}}" style="margin-right: 20px;text-decoration: none">Sign up</a>
            <a href="{{route('auth.login')}}" style="margin-right: 20px;text-decoration: none">Sign in</a>
        @endif
    </div>
</nav>
<div class="container">

    @yield('content')
</div>
</body>
</html>
