@extends('layouts.main')

@section('content')

    @if(session('cart'))
        <table class="table mt-5">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Available Publications</th>
            </tr>
            </thead>
            <tbody>

                <tr>
                    <th scope="row">{{$cart['id']}}</th>
                    <td>{{$cart['name']}}</td>
                    <td>{{$cart['price']}}</td>
                    <td>{{$cart['available_publications']}}</td>
                    <td>
                        <form action="{{route('cart.delete',$cart['id'])}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>

            </tbody>
        </table>

        <div class="mt-5">
            <a href="{{route('cart.paymentMethod')}}">
                <button class="btn btn-success">Payment</button>
            </a>
        </div>

    @endif

@endsection
