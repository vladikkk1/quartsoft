@extends('layouts.main')


@section('content')

    <div class="container" style="margin-top: 100px">
        <h4>Payment Systems</h4>
        <div class="d-flex justify-content-start">
            <form action="{{route('make.payment')}}" method="post">
                @csrf
                <button name="payment_system" value="PayPal" class="btn btn-primary" style="margin-right: 10px">Checkout PayPal</button>
                <button name="payment_system" value="TwoCheckout" class="btn btn-success">Checkout 2Checkout</button>
            </form>
        </div>
    </div>
@endsection
