@extends('layouts.main')



@section('content')

    <div class="container mt-5">
        <a href="{{route('publication.index')}}">Publications List</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Plan</th>
                <th scope="col">Available Publications</th>
                <th scope="col">Remains</th>
                <th scope="col">Date Start</th>
                <th scope="col">Date End</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">{{$plan->id}}</th>
                <td>{{$plan->name}}</td>
                <td>{{$plan->available_publications}}</td>
                <td>{{$remainsPublication}}</td>
                <td>{{$plan->end_date}}</td>
                <td>{{$plan->end_date}}</td>
        </tr>
        </tbody>
    </table>
</div>
@endsection
