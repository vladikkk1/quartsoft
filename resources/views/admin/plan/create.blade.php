@extends('layouts.admin')


@section('admin_content')

    <div class="mt-5">
        <form action="{{route('admin.plan.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Plan Name</label>
                <input type="text" class="form-control" name="name" value="{{old('name')}}" id="exampleInputEmail1" aria-describedby="emailHelp"
                >
                @error('name')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group mt-3">
                <label for="exampleInputPassword1">Plan Price</label>
                <input type="text" class="form-control" value="{{old('price')}}" name="price" >
                @error('price')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group mt-3">
                <label for="exampleInputPassword1">Plan Available Publications</label>
                <input type="number" class="form-control" value="{{old('available_publications')}}" name="available_publications" >
                @error('available_publications')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group mt-3">
                <label for="exampleInputPassword1">Plan Price</label>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input"  type="radio" checked name="active" id="inlineRadio1"
                           value="1">
                    <label class="form-check-label" for="inlineRadio1">Active</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="active" id="inlineRadio2"
                           value="0">
                    <label class="form-check-label" for="inlineRadio2">Inactive</label>
                </div>
            </div>
            <button  type="submit" class="btn btn-primary  mt-3">Submit</button>
        </form>
    </div>
@endsection
