@extends('layouts.admin')


@section('admin_content')

    <div class="mt-5">
        <a href="{{route('admin.plan.create')}}">Create Plan</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Available Publications</th>
                <th scope="col">Active Status</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($plans_list as $plan)
                <tr>
                    <th scope="row">{{$plan->id}}</th>
                    <td>{{$plan->name}}</td>
                    <td>{{$plan->price}}</td>
                    <td>{{$plan->available_publications}}</td>
                    <td>{{$plan->active ? 'Active' : 'InActive'}}</td>
                    <td>
                        <form action="{{route('admin.plan.changeStatus' , $plan->id)}}" method="post">
                            @csrf
                            <button class="btn btn-primary">Change Status</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
