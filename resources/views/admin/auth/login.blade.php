@extends('layouts.admin')


@section('admin_content')
    <div class="mt-5">
        <form action="{{route('admin.auth.login.store')}}" method="post">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">admin@gmail.com</small>
                @error('email')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>

            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                <small id="emailHelp" class="form-text text-muted">admin</small>
                @error('password')
                <p style="color: red">{{$message}}</p>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
