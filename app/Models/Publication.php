<?php

namespace App\Models;

use App\Models\Traits\Publicationable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    use HasFactory;
    use Publicationable;

    protected $guarded = false;
}
