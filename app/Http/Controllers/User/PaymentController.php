<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Payment\PaymentSystemRequest;
use App\Services\Payment\PaymentFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{


    public function payment(PaymentSystemRequest $paymentSystemRequest)
    {

        $paymentSystem = $paymentSystemRequest->validated();

        $cart = session()->get('cart');

        $payment = PaymentFactory::create($paymentSystem['payment_system']);
        return $payment->payment(Auth::id(), $cart);


    }


    public function success(Request $request)
    {

        $data = $request->all();
        $system = 'TwoCheckout';

        if (array_key_exists('PayerID', $data)) {
            $system = 'PayPal';
        }

        $payment = PaymentFactory::create($system);
        $payment->success($data);

        return redirect()->route('home');
    }


}
