<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Publication\PublicationChangeStatusRequest;
use App\Http\Requests\Publication\StoreRequest;
use App\Models\Publication;
use App\Models\User;
use App\Services\User\PublicationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicationController extends Controller
{

    private PublicationService $publicationService;

    public function __construct()
    {
        $this->publicationService = new PublicationService();

    }

    public function index()
    {

        $publicationsList = $this->publicationService->publicationsListUser(Auth::id());
        $user = Auth::user();

        return view('publication.index', compact('publicationsList', 'user'));
    }

    public function create()
    {
        return view('publication.create');
    }


    public function store(StoreRequest $storeRequest)
    {
        $data = $storeRequest->validated();

        $user = Auth::user();
        $data['user_id'] = $user->id;
        $data['subscription_plan_id'] = $user->active_plan_id;
        $this->publicationService->createPublication($data);

        return redirect()->route('cabinet.index');

    }


    public function changeStatus(PublicationChangeStatusRequest $publicationChangeStatusRequest, Publication $publication)
    {
        $data = $publicationChangeStatusRequest->validated();
        $this->publicationService->changeStatusPublication($publication);
        return back();
    }


    public function show(Publication $publication)
    {
        return view('publication.show', compact('publication'));
    }

}
