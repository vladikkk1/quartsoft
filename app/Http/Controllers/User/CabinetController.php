<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\User\PublicationService;
use App\Services\User\UserSubscriptionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CabinetController extends Controller
{

    private UserSubscriptionService $userSubscriptionService;
    private PublicationService $publicationService;

    public function __construct()
    {
        $this->userSubscriptionService = new UserSubscriptionService();
        $this->publicationService = new PublicationService();
    }


    public function index()
    {

        $user = Auth::user();

        if (!UserSubscriptionService::checkExistsSubscription($user->id)) {
            return redirect()->route('publication.index');
        }

        $plan = $this->userSubscriptionService->activeSubscriptionPlanData($user->active_plan_id);

        $publicationActiveCount = $this->publicationService->userActivePublicationCount($user->id, $user->active_plan_id);

        $remainsPublication = $plan->available_publications - $publicationActiveCount;

        return view('cabinet.index', compact('plan', 'remainsPublication'));
    }
}
