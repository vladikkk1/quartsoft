<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Filters\PublicationFilter;
use App\Http\Requests\Filter\PublicationFilterRequest;
use App\Models\Publication;
use App\Models\SubscriptionPlan;
use App\Models\UserSubscription;
use App\Services\User\PublicationService;

class SiteController extends Controller
{

    private PublicationService $publicationService;

    public function __construct()
    {
        $this->publicationService = new PublicationService();
    }

    public function index(PublicationFilterRequest $publicationFilterRequest)
    {

        $data = $publicationFilterRequest->validated();
        $filter = app()->make(PublicationFilter::class, ['queryParams' => array_filter($data)]);

        $publications = $this->publicationService->publicationFilters($filter);

        $users = $this->publicationService->publicationUsers();


        return view('publications', compact('publications', 'users'));
    }

    public function plans()
    {

        $allPlans = SubscriptionPlan::query()->where('active', '=', '1')->get();

        return view('plans', compact('allPlans'));
    }

    public function publicationShow(Publication $publication)
    {
        return view('publication.show', compact('publication'));
    }
}
