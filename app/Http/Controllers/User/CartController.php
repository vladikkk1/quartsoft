<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionPlan;
use App\Services\Cart\CartService;

class CartController extends Controller
{

    private CartService $cartService;

    public function __construct()
    {
        $this->cartService = new CartService();
    }

    public function index()
    {

        $cart = session('cart');
        return view('cart.index', compact('cart'));
    }

    public function add(SubscriptionPlan $plan)
    {
        $this->cartService->addToCart($plan);

        return redirect()->route('cart.index');
    }


    public function delete(SubscriptionPlan $plan)
    {
        $this->cartService->remove($plan->id);

        return redirect()->route('home');
    }


    public function paymentMethod()
    {
        return view('cart.paymentMethod');
    }
}
