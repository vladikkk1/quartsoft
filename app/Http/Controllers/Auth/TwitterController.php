<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class TwitterController extends Controller
{


    public function loginwithTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function cbTwitter()
    {
        try {

            $user = Socialite::driver('twitter')->user();

            $userWhere = User::where('twitter_id', $user->id)->first();

            if ($userWhere) {

                UserService::authenticateUser($userWhere);

                return redirect()->route('home');

            } else {

                if (isset($user->email)) {
                    $gitUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'twitter_id' => $user->id,
                        'oauth_type' => 'twitter',
                        'password' => encrypt('password')
                    ]);

                    UserService::authenticateUser($gitUser);

                    return redirect()->route('home');
                }

                return redirect()->route('auth.login')->with('error', 'Twitter did not provide access to the mail!');

            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
