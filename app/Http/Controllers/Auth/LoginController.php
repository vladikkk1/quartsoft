<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginStoreRequest;
use App\Models\User;
use App\Services\User\UserService;
use App\Services\User\UserSubscriptionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    private UserService $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function login()
    {
        return view('auth.login');
    }


    public function store(LoginStoreRequest $loginStoreRequest)
    {
        $data = $loginStoreRequest->validated();

        if ($this->userService->loginUserWithForm($data)) {

            return redirect()->route('home');
        }

    }
}
