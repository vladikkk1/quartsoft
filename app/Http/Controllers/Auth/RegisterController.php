<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterStoreRequest;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{

    private UserService $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function register()
    {
        return view('auth.register');
    }


    public function store(RegisterStoreRequest $registerStoreRequest)
    {
        $data = $registerStoreRequest->validated();

        $user = $this->userService->createUser($data);
        if ($user) {
            Auth::login($user);
        }

        return redirect()->route('home');
    }






}
