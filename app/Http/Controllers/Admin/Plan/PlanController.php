<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Plan\ChangeStatusRequest;
use App\Http\Requests\Admin\Plan\InActiveRequest;
use App\Http\Requests\Admin\Plan\StoreRequest;
use App\Models\SubscriptionPlan;
use App\Services\Admin\PlanService;
use Illuminate\Http\Request;

class PlanController extends Controller
{

    private PlanService $planService;

    public function __construct()
    {
        $this->planService = new PlanService();
    }

    public function index()
    {
        $plans_list = SubscriptionPlan::all();

        return view('admin.plan.index', compact('plans_list'));
    }

    public function create()
    {
        return view('admin.plan.create');
    }

    public function store(StoreRequest $storeRequest)
    {
        $data = $storeRequest->validated();
        $newPlan = $this->planService->createPlan($data);

        if ($newPlan) {
            return redirect()->route('admin.plan.index');
        }

        return back();
    }


    public function changeStatus(SubscriptionPlan $plan)
    {

        $change = $this->planService->changeStatusPlan($plan);
        return back();
    }


}
