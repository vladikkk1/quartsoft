<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginRequest;

use App\Services\Admin\AdminService;


class LoginController extends Controller
{


    private AdminService $adminUser;

    public function __construct()
    {
        $this->adminUser = new AdminService();
    }

    public function login()
    {
        return view('admin.auth.login');
    }

    public function store(LoginRequest $loginRequest)
    {

        $data = $loginRequest->validated();
        $adminUser = $this->adminUser->authenticateAdmin($data);
        if ($adminUser) {
            return redirect()->route('admin.plan.index');
        }
        return redirect()->route('admin.auth.login');
    }
}
