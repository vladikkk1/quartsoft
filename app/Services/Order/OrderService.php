<?php

namespace App\Services\Order;

use App\Models\Order;

class OrderService
{

    public function createOrder($payment_id, $user_id, $cart, $system)
    {
        $order = Order::create(
            [
                'payment_id' => $payment_id,
                'user_id' => $user_id,
                'plan_id' => $cart['id'],
                'price' => $cart['price'],
                'payment_gateway' => $system,
                'order_status' => 'process'
            ]
        );

        return $order;
    }


    public function OrderStatusCompleted($payment_id)
    {

        $up = Order::query()
            ->where('payment_id', '=', "$payment_id")
            ->update(['order_status' => 'completed']);

        return $up;
    }


    public function getOrder($payment_id)
    {
        $order = Order::query()
            ->where('payment_id', '=', "$payment_id")
            ->where('order_status', '=', 'completed')
            ->first();

        return $order;
    }


}
