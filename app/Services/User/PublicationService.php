<?php

namespace App\Services\User;

use App\Models\Publication;

class PublicationService
{

    public function userActivePublicationCount($user_id, $active_plan_id)
    {

        return Publication::query()
            ->where('subscription_plan_id', '=', $active_plan_id)
            ->where('user_id', '=', $user_id)
            ->where('status', '=', 'active')
            ->count('id');

    }

    public function publicationsListUser($user_id)
    {
        return Publication::query()
            ->where('user_id', '=', $user_id)
            ->get();
    }


    public function createPublication($data)
    {
        return Publication::create($data);
    }


    public function changeStatusPublication(Publication $publication)
    {

        $newStatus = $publication->status == 'active' ? 'draft' : 'active';

        return Publication::query()
            ->where('id', '=', $publication->id)
            ->update([
                'status' => $newStatus
            ]);

    }


    public function publicationFilters($filter)
    {

        return Publication::query()
            ->join('users', 'users.id', '=', 'publications.user_id')
            ->filter($filter)
            ->where('status', '=', 'active')
            ->orderBy('id', 'desc')
            ->select('publications.*', 'users.name')
            ->get();

    }


    public function publicationUsers()
    {
        return Publication::query()
            ->join('users', 'users.id', '=', 'publications.user_id')
            ->where('status', '=', 'active')
            ->selectRaw("distinct users.name")
            ->get();
    }

}
