<?php

namespace App\Services\User;

use App\Models\User;
use App\Models\UserSubscription;
use Illuminate\Support\Facades\DB;

class UserSubscriptionService
{


    public function createUserSubscription($order)
    {

        $create = UserSubscription::create(
            [
                'user_id' => $order['user_id'],
                'subscription_plan_id' => $order['plan_id'],
                'start_date' => date('Y-m-d H:i:s'),
                'end_date' => date("Y-m-d H:i:s", strtotime("+1 month"))
            ]
        );

        return $create;
    }


    public function activeSubscriptionPlanData($plan_id)
    {

        return UserSubscription::query()
            ->join('subscription_plans', 'subscription_plans.id', '=', 'user_subscriptions.subscription_plan_id')
            ->where('user_subscriptions.subscription_plan_id', '=', $plan_id)
            ->select('user_subscriptions.*', 'subscription_plans.name', 'subscription_plans.available_publications')
            ->where('status', '=', true)
            ->first();

    }


    public static function checkExpirationDateSubscription($user_id)
    {

        if (self::checkExistsSubscription($user_id)) {
            DB::transaction(function () use ($user_id) {

                User::query()
                    ->where('id', '=', $user_id)
                    ->update(['active_plan_id' => null]);

                UserSubscription::query()
                    ->where('user_id', '=', $user_id)
                    ->where('status', '=', true)
                    ->update(['status' => false]);
            });
        }
    }


    public static function checkExistsSubscription($user_id)
    {

        return UserSubscription::query()
            ->where('user_id', '=', $user_id)
            ->where('status', '=', true)
            ->exists();
    }


}
