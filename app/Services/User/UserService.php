<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserService
{

    public function createUser($data): User|bool
    {

        $data['password'] = encrypt($data['password']);
        $user = User::create($data);
        if ($user) {
            return $user;
        }
        return false;
    }

    public function loginUserWithForm($data): bool
    {
        $user = User::query()
            ->where('email', '=', $data['email'])
            ->first();

        self::authenticateUser($user);

        return true;
    }


    public static function authenticateUser($user)
    {

        UserSubscriptionService::checkExpirationDateSubscription($user['id']);

        Auth::login($user);

    }


    public function setActivePlan($user_id, $plan_id)
    {
        $activePlan = User::query()
            ->where('id', '=', $user_id)
            ->update(['active_plan_id' => $plan_id]);

        return $activePlan;
    }


}
