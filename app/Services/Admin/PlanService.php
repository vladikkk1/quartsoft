<?php

namespace App\Services\Admin;

use App\Models\SubscriptionPlan;

class PlanService
{

    public function createPlan($data): SubscriptionPlan|bool
    {

        $plan = SubscriptionPlan::create($data);
        if ($plan) {
            return $plan;
        }
        return false;
    }


    public function changeStatusPlan(SubscriptionPlan $plan): SubscriptionPlan|bool
    {
        $change = $plan->update(
            [
                'active' => !$plan->active
            ]
        );

        if ($change) {
            return $change;
        }

        return false;

    }

}
