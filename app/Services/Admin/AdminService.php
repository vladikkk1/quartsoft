<?php

namespace App\Services\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AdminService
{

    public function authenticateAdmin($data): ?bool
    {
        $user = User::query()
            ->where('email', '=', $data['email'])
            ->where('is_admin', '=', true)
            ->first();

        if ($user) {
            if (decrypt($user->password) == $data['password']) {

                Auth::login($user);
                return true;
            }
        }
        return false;
    }


}
