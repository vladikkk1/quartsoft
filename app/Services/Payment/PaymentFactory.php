<?php

namespace App\Services\Payment;

use App\Services\Payment\PayPalExpress\PayPalExpressService;
use App\Services\Payment\TwoCheckout\TwoCheckoutService;

class PaymentFactory
{

    public static function create($gateway)
    {
        switch ($gateway) {
            case 'PayPal':
                return new PayPalExpressService();
            case 'TwoCheckout':
                return new TwoCheckoutService();
            default:
                throw new \Exception('Invalid payment gateway');
        }
    }

}
