<?php

namespace App\Services\Payment\PayPalExpress;

use App\Models\UserSubscription;
use App\Services\Order\OrderService;
use App\Services\Payment\PaymentInterface;
use App\Services\User\UserService;
use App\Services\User\UserSubscriptionService;
use Omnipay\Omnipay;

class PayPalExpressService implements PaymentInterface
{

    private $gateaway;
    private OrderService $orderService;
    private UserSubscriptionService $subscribedService;
    private UserService $userService;

    public function __construct()
    {
        $this->gateaway = Omnipay::create('PayPal_Rest');
        $this->gateaway->setClientId(env('PAYPAL_SANDBOX_API_CLIENT'));
        $this->gateaway->setSecret(env('PAYPAL_SANDBOX_API_SECRET'));
        $this->gateaway->setTestMode(true);

        $this->orderService = new OrderService();
        $this->subscribedService = new UserSubscriptionService();
        $this->userService = new UserService();
    }

    public function payment($user_id, $cart)
    {


        try {

            $response = $this->gateaway->purchase(array(
                'amount' => $cart['price'],
                'currency' => env('PAYPAL_CURRENCY'),
                'returnUrl' => route('make.success'),
                'cancelUrl' => route('home')
            ));

            $response = $response->send();

            $this->orderService->createOrder($response->getTransactionReference(), $user_id, $cart, 'Paypal', 'process');

            if ($response->isRedirect()) {
                $response->redirect();
            } else {
                return $response->getMessage();
            }

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }


    public function success($data)
    {

        $transaction = $this->gateaway->completePurchase(array(
            'payer_id' => $data['PayerID'],
            'transactionReference' => $data['paymentId']
        ));

        $response = $transaction->send();

        if ($response->isSuccessful()) {

            $arr = $response->getData();

            $updateOrderStatusToCompleted = $this->orderService->OrderStatusCompleted($arr['id']);

            if ($updateOrderStatusToCompleted) {

                $order = $this->orderService->getOrder($arr['id']);

                $this->subscribedService->createUserSubscription($order);
                $this->userService->setActivePlan($order->user_id, $order->plan_id);

                session()->forget('cart');
            }

        }

    }
}
