<?php

namespace App\Services\Payment\TwoCheckout;

use App\Services\Order\OrderService;
use App\Services\Payment\PaymentInterface;

use App\Services\Payment\TwoCheckout\src\Tco\TwocheckoutFacade;
use App\Services\User\UserService;
use App\Services\User\UserSubscriptionService;

class TwoCheckoutService implements PaymentInterface
{


    private int $sellerId;
    private string $secretKey;

    private TwocheckoutFacade $tco;

    private OrderService $orderService;
    private UserService $userService;
    private UserSubscriptionService $userSubscriptionService;

    public function __construct()
    {
        $this->sellerId = env('TWO_CHECKOUT_SELLER_ID');
        $this->secretKey = env('TWO_CHECKOUT_SECRET_KEY');
        $this->tco = new TwocheckoutFacade([
            'sellerId' => $this->sellerId, // REQUIRED
            'secretKey' => $this->secretKey, // REQUIRED
            'jwtExpireTime' => 30,
            'curlVerifySsl' => 1
        ]);

        $this->orderService = new OrderService();
        $this->userService = new UserService();
        $this->userSubscriptionService = new UserSubscriptionService();

    }

    public function payment($user_id, $cart)
    {


        $order = $this->tco->order();

        $dynamicProductParamsSuccess = array(
            'Country' => 'us',
            'Currency' => 'USD',
            'CustomerIP' => '91.220.121.21',
            'ExternalReference' => 'CustOrd100',
            'Language' => 'en',
            'Source' => 'tcolib.local',
            'BillingDetails' =>
                array(
                    'Address1' => 'Test Address',
                    'City' => 'LA',
                    'State' => 'California',
                    'CountryCode' => 'US',
                    'Email' => 'testcustomer@2Checkout.com',
                    'FirstName' => 'Customer',
                    'LastName' => '2Checkout',
                    'Zip' => '12345',
                ),
            'Items' =>
                array(
                    0 =>
                        array(
                            'Name' => $cart['name'],
                            'Description' => 'Test description',
                            'Quantity' => 1,
                            'IsDynamic' => true,
                            'Tangible' => false,
                            'PurchaseType' => 'PRODUCT',
                            'Price' =>
                                array(
                                    'Amount' => $cart['price'], //value
                                    'Type' => 'CUSTOM',
                                ),
                        )
                ),
            'PaymentDetails' =>
                array(
                    'Type' => 'TEST', //test mode
                    'Currency' => 'USD',
                    'CustomerIP' => '91.220.121.21',
                    'PaymentMethod' =>
                        array(
                            'CardNumber' => '378282246310005',
                            'CardType' => 'AMEX',
                            'Vendor3DSReturnURL' => route('make.success'),
                            'Vendor3DSCancelURL' => 'www.fail.com',
                            'ExpirationYear' => '2023',
                            'ExpirationMonth' => '12',
                            'CCID' => '123',
                            'HolderName' => 'John Doe',
                            'RecurringEnabled' => false,
                            'HolderNameTime' => 1,
                            'CardNumberTime' => 1,
                        ),
                ),
        );


        $response = $order->place($dynamicProductParamsSuccess);

        $this->orderService->createOrder($response['RefNo'], $user_id, $cart, 'TwoCheckout');

        return redirect()->route('make.success', compact('response'));

    }

    public function success($data)
    {
        $response = $data['response'];
        $orderData = $this->tco->order()->getOrder(array('RefNo' => $response['RefNo']));

        $orderStatusCompleted = $this->orderService->OrderStatusCompleted($orderData['RefNo']);

        if ($orderStatusCompleted) {
            $order = $this->orderService->getOrder($orderData['RefNo']);

            $this->userSubscriptionService->createUserSubscription($order);
            $this->userService->setActivePlan($order->user_id, $order->plan_id);

            session()->forget('cart');
        }

    }
}
