<?php

namespace App\Services\Payment\TwoCheckout\src\Tco\Source\BuyLinkSignature;

use App\Services\Payment\TwoCheckout\src\Tco\Exceptions\TcoException;
use App\Services\Payment\TwoCheckout\src\Tco\Source\Api\Rest\V6\ApiCore;
use App\Services\Payment\TwoCheckout\src\Tco\Source\TcoConfig;
use App\Services\Payment\TwoCheckout\src\Tco\Source\Api\Auth\AuthFactory;
use App\Services\Payment\TwoCheckout\src\Tco\Source\BuyLinkSignature\Jwt\JwtGenerator;

class BuyLinkSignatureGenerator {

    /**
     * @var TcoConfig
     */
    private $tcoConfig;

    /**
     * BuyLinkSignatureGenerator constructor.
     *
     * @param TcoConfig $tcoConfig
     */
    public function __construct( $tcoConfig ) {
        $this->tcoConfig = $tcoConfig;
    }

    /**
     * @param array $params
     *
     * @return string|null
     * @throws \TcoException
     */
    public function generateSignature( $params ) {
        try {
            $auth = ( new AuthFactory( $this->tcoConfig ) )->getAuth( 'BuyLink' );
            //Generate the token now
            $jwtGenerator = new JwtGenerator( $this->tcoConfig );
            $token        = $jwtGenerator->generateToken();
            $auth->setMerchantToken( $token );

            $api      = new ApiCore( $this->tcoConfig, $auth );
            $response = $this->doApiRequest( $api, $params );
            if ( isset( $response['signature'] ) ) {
                return $response['signature'];
            } else {
                throw new TcoException( sprintf('Api response is missing Signature parameter: ,%s', print_r($response,
                    true)) );
            }
        } catch ( TcoException $exception ) {
            throw new TcoException(  sprintf( 'Exception generating buy link signature: %s', $exception->getMessage() ) );
        }
    }

    /**
     * @param ApiCore $api
     * @param array $params
     *
     * @return array
     * @throws \TcoException
     */
    private function doApiRequest( $api, $params ) {
        try {
            return $api->call( '', $params, 'POST', 'buyLink' );
        }
        catch (TcoException $exception){
            throw new TcoException($exception->getMessage());
        }

    }

}
