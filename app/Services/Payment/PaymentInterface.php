<?php

namespace App\Services\Payment;

interface PaymentInterface
{

    public function payment($user_id , $cart);
    public function success($data);

}
