<?php

namespace App\Services\Cart;

use App\Models\SubscriptionPlan;

class CartService
{

    public function addToCart(SubscriptionPlan $plan)
    {

        $cart = session()->get('cart');

        $cart = [
            'id' => $plan->id,
            'name' => $plan->name,
            'price' => $plan->price,
            'available_publications' => $plan->available_publications,
            'count' => 1
        ];

        session()->put('cart', $cart);

        return session()->get('cart');
    }


    public function remove($id)
    {
        $cart = session()->get('cart');

        if ($cart['id'] == $id) {
            session()->forget('cart');
        }

        return true;
    }

}
