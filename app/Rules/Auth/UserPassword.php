<?php

namespace App\Rules\Auth;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Auth;

class UserPassword implements ValidationRule, DataAwareRule
{
    protected $data = [];

    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $user = User::query()
            ->where('email', '=', $this->data['email'])
            ->first();

        if ($user) {
            if (decrypt($user->password) != $this->data['password']) {
                $fail('Wrong password!');
            }
        } else {
            $fail('Wrong password!');
        }

    }

    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }
}
