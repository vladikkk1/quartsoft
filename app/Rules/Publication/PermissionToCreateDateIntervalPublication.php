<?php

namespace App\Rules\Publication;

use App\Services\User\UserSubscriptionService;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Auth;

class PermissionToCreateDateIntervalPublication implements ValidationRule
{


    private UserSubscriptionService $userSubscriptionService;

    public function __construct()
    {
        $this->userSubscriptionService = new UserSubscriptionService();
    }


    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $user = Auth::user();
        $planActiveUser = $this->userSubscriptionService->activeSubscriptionPlanData($user->active_plan_id);
        $dateNow = date('Y-m-d H:i:s');
        if ($planActiveUser->start_date > $dateNow or $planActiveUser->end_date < $dateNow) {
            $fail('Creation is not available! Creation time is not included in the interval!');
        }


    }
}
