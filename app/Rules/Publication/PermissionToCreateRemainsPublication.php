<?php

namespace App\Rules\Publication;

use App\Services\User\PublicationService;
use App\Services\User\UserSubscriptionService;
use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Support\Facades\Auth;

class PermissionToCreateRemainsPublication implements ValidationRule, DataAwareRule
{

    protected $data = [];

    private UserSubscriptionService $userSubscriptionService;
    private PublicationService $publicationService;

    public function __construct()
    {
        $this->publicationService = new PublicationService();
        $this->userSubscriptionService = new UserSubscriptionService();
    }

    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        if ($this->data['status'] == 'active') {
            $user = Auth::user();
            $plan = $this->userSubscriptionService->activeSubscriptionPlanData($user->active_plan_id);
            $publicationActiveCount = $this->publicationService->userActivePublicationCount($user->id, $user->active_plan_id);

            $remainsPublication = $plan->available_publications - $publicationActiveCount;

            if ($remainsPublication <= 0) {
                $fail('The limit of publications has ended!');
            }
        }
    }

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}
